<?php
  require "/bd.php";
  include("include/db_connect.php");
    if ($_POST["save_setting"]) {
        $error = array();

        $pass = $_POST['profile_password'];
        if ($_SESSION['user_pass'] != $pass) {
            $error[] = "Неверный текущий пароль";
        } else {
            if ($_POST['profile_new_password'] != "") {
                if (strlen($_POST['profile_new_password']) < 4) {
                    $error[] = "Укажите новый пароль  не меньше 4 символов";
                } else {
                    $new_pass = password_hash($_POST['profile_new_password'], PASSWORD_DEFAULT);
                }
            }

            if (strlen($_POST['profile_FIO']) < 3) {
                $error[] = "Укажите ФИО от 3 до 100 символов";
            }

            if (strlen($_POST['profile_phone']) == "") {
                $error[] = "Укажите телефон";
            }

            if (strlen($_POST['profile_address']) == "") {
                $error[] = "Укажите адрес доставки";
            }
        }

        if (count($error)) {
            echo '<div style="color: red;">'.implode($error).'</div><hr>';
        } else {
            echo '<div style="color: yellow;">Данные успешно сохранены.</div><hr>';
            $update = mysqli_query($connection, "UPDATE `users` SET `email`='".$_POST['profile_email']."', `password`='".$new_pass."', `fullname`='".$_POST['profile_FIO']."', `address`='".$_POST['profile_address']."', `phone`='".$_POST['profile_phone']."' WHERE  login='".$_SESSION['user_login']."' ");
        }
    }
 ?>
 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>Profile</title>
     <link rel="stylesheet" href="css/style.css">
     <link rel="stylesheet" href="/fonts/fonts.css">
     <script src="/js/jquery-3.1.1.min.js"></script>
     <script src="/js/shop-script.js"></script>
   </head>
   <body>
    <header>
      <?php
      include("/header.php");
       ?>
     </header>
    <content>

      <form action="/profile.php" method="POST">
        <ul id="profile_info">
         <h3>Изменение профиля</h3>
          <li>
            <label for="">Текущий пароль</label>
            <input type="password" name="profile_password">
          </li>
            <li>
              <label for="">Новый пароль</label>
              <input type="password" name="profile_new_password">
            </li>
              <li>
                <label for="">ФИО</label>
                <input type="text" name="profile_FIO"  value="<?php echo @$_POST['profile_FIO']; ?>">
              </li>
                <li>
                  <label for="">Email</label>
                  <input type="email" name="profile_email"  value="<?php echo @$_POST['profile_email']; ?>">
                </li>
                  <li>
                    <label for="">Адрес</label>
                    <textarea name="profile_address"  value="<?php echo @$_POST['profile_address']; ?>"></textarea>
                  </li>
                    <li>
                      <label for="">Телефон</label>
                      <input type="phone" name="profile_phone"  value="<?php echo @$_POST['profile_phone']; ?>">
                    </li>
        </ul>
        <input type="submit" id="save_setting" name="save_setting"></input>
      </form>
    </content>
   </body>
 </html>

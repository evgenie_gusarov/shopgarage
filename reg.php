<?php
require("/bd.php");
include("include/db_connect.php");
$data = $_POST;
if (isset($data['bd_reg'])) {
    //Здесь регистрируем

    $errors = array();
    if (trim($data['fullname']) == '') {
        $errors[]="Введите ФИО!";
    }
    if (trim($data['login']) == '') {
        $errors[]="Введите логин!";
    }
    if (trim($data['email']) == '') {
        $errors[]="Введите E-mail!";
    }
    if (trim($data['address']) == '') {
        $errors[]="Введите адрес!";
    }
    if (trim($data['phone']) == '') {
        $errors[]="Введите телефон!";
    }
    if (trim($data['password_2']) != $data['password']) {
        $errors[]="Повторный пароль введен не верно!";
    }

    if (R::count('users', "login = ?", array($data['login'])) > 0) {
        $errors[]="Пользователь с таким логином уже существует!";
    }

    if (R::count('users', "email = ?", array($data['email'])) > 0) {
        $errors[]="Пользователь с таким E-mail уже существует!";
    }

    if (empty($errors)) {
        $user = R::dispense('users');
        $user ->fullname = $data['fullname'];
        $user->login = $data['login'];
        $user->email = $data['email'];
        $user->address = $data['address'];
        $user->phone = $data['phone'];
        $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
        R::store($user);
        ob_start();
        echo '<script>alert("Вы успешно зарегистрировались");</script>';
        header('Location: /index.php');
    } else {
        echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>garage</title>
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/authorization_registration.css">
    <link rel="stylesheet" href="/fonts/fonts.css">
    <script type="text/javascript" src="trackbar/jQuery/jquery-1.2.3.min.js"></script>
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="trackbar/jQuery/jquery.trackbar.js"></script>
</head>
<body>
<header>
    <?php
    include("/header.php");
    ?>
</header>

<div id="fon">
    <ul class="body_slides">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
    <form id="reg" action="/reg.php" method="POST">
        <p>
        <p><strong>ФИО</strong></p>
        <fieldset id="inputs_reg">
            <input id="fullname" type="text" name="fullname" value="<?php echo @$data['fullname']; ?>" placeholder="Введите ФИО" required>
        </fieldset>
        </p>
        <p>
        <p><strong>Ваш логин</strong></p>
        <fieldset id="inputs_reg">
            <input id="username" type="text" name="login" value="<?php echo @$data['login']; ?>" placeholder="Введите ваш логин" required>
        </fieldset>
        </p>

        <p>
        <p><strong>Ваш Email</strong></p>
        <fieldset id="inputs_reg">
            <input id="email" type="email" name="email" value="<?php echo @$data['email']; ?>" placeholder="Введите ваш E-mail" required>
        </fieldset>
        </p>
        <p>
        <p><strong>Ваш адресс</strong></p>
        <fieldset id="inputs_reg">
            <input id="address" type="text" name="address" value="<?php echo @$data['address']; ?>" placeholder="Введите ваш адрес" required>
        </fieldset>
        </p>
        <p><strong>Ваш Телефон</strong></p>
        <fieldset id="inputs_reg">
            <input id="phone" type="phone" name="phone" value="<?php echo @$data['phone']; ?>" placeholder="Введите ваш телефон" required>
        </fieldset>
        </p>

        <p>
        <p><strong>Ваш пароль</strong></p>
        <fieldset id="inputs_reg">
            <input id="password" type="password" name="password" value="<?php echo @$data['password']; ?>" placeholder="Введите ваш пароль" required>
        </fieldset>
        </p>

        <p>
        <p><strong>Повторите ваш пароль</strong></p>
        <fieldset id="inputs_reg">
            <input id="password" type="password" name="password_2" value="<?php echo @$data['password_2']; ?>" placeholder="Повторите ваш пароль" required>
        </fieldset>
        </p>

        <p>
        <fieldset id="actions">
            <button id="submit-reg" type="submit" name="bd_reg">Зарегестрироваться</button>
        </fieldset>
        </p>

    </form>
</div>
<script src="/js/jquery-3.1.1.min.js"></script>
<script src="/js/bucket.js"></script>
</body>
</html>

<?php
require "/bd.php";
include("include/db_connect.php");
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Интернет-магазин "Гараж.ру"</title>
    <meta charset = "utf-8">
    <link rel="stylesheet" type="text/css" href="css/store_location.css">
    <link rel="stylesheet" type="text/css" href="css/header.css">
	<link rel="stylesheet" type="text/css" href="/fonts/fonts.css">
    <script type="text/javascript" src="trackbar/jQuery/jquery-1.2.3.min.js"></script>
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="trackbar/jQuery/jquery.trackbar.js"></script>
</head>
<body>

<header>
	<?php
        include("/header.php");
    ?>
</header>

		<div id="content_center">
		<h3 id = "title_oplata">Пункты выдачи</h3>

			<div id="PointsOfIssue">
				<p>Мы находимся по адресам:</p>
				<div id="address-block">
					<p>г.Пенза, ул.Пушкина д.137, к.1</p>
					<p><b>Телефон:</b>
					+7(8412)54-00-28</p>
					<p><b>Часы работы:</b><time>09:00 - 21:00</time></p>
				</div>
				<div id="address-block">
					<p>г.Пенза, ул.Московская д.73</p>
					<p><b>Телефон:</b>
					+7(8412)75-75-85</p>
					<p><b>Часы работы:</b><time>09:00 - 21:00</time></p>
				</div>
				<div id="address-block">
					<p>г.Пенза, ул.Рахманинова д.1</p>
					<p><b>Телефон:</b>
					+7(8412)50-50-20</p>
					<p><b>Часы работы:</b><time>09:00 - 21:00</time></p>
				</div>
			</div>

			<div id="maps">
				<iframe src="https://api-maps.yandex.ru/frame/v1/-/CZsgUJOJ" width="560" height="400" frameborder="0"></iframe>
			</div>
		</div>
</body>
</html>

<?php
require "/bd.php";
include("/include/db_connect.php");
include("/function/function.php");
session_start();


$result = mysqli_query($connection, "SELECT * FROM `users` WHERE login='".$_SESSION['logged_user']->login."'");
if (mysqli_num_rows($result) > 0) {
    $rows = mysqli_fetch_array($result);
    $_SESSION['data_login']=$rows["login"];
    $_SESSION['data_email']=$rows["email"];
    $_SESSION['data_fullname']=$rows["fullname"];
    $_SESSION['data_address']=$rows["address"];
    $_SESSION['data_phone']=$rows["phone"];
}

$id = clear_string($_GET["id"]);
$action = clear_string($_GET["action"]);

$result = mysqli_query($connection, "SELECT * FROM `cart` WHERE 1");
$row = mysqli_fetch_array($result);
$cart_id = $row["cart_id"];
$count_products = $row["count_products"] - 1;

switch ($action) {
    case 'clear':
        $clear = mysqli_query($connection, "DELETE FROM `cart` WHERE cart_id ");
        break;
    case 'delete':
        if ($row == 0) {
            $delete = mysqli_query($connection, "UPDATE `cart` SET `count_products` = ' $count_products' WHERE cart_id` = '".$id."'");
            break;
        }
}

if (isset($_POST["submitdata"])) {
    $_SESSION["order_delivery"] = $_POST["order_delivery"];
    $_SESSION["order_fio"] = $_POST["order_fio"];
    $_SESSION["order_email"] = $_POST["order_email"];
    $_SESSION["order_phone"] = $_POST["order_phone"];
    $_SESSION["order_address"] = $_POST["order_address"];

    header("Location: cart.php?action=completion");
}
?>
<!doctype html>
<html>
<head>
    <title>Корзина заказов</title>
    <meta charset = "utf-8">
    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" type="text/css" href="css/basket.css">
    <link rel="stylesheet" type="text/css" href="/fonts/fonts.css">
    <script type="text/javascript" src="trackbar/jQuery/jquery-1.2.3.min.js"></script>
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="trackbar/jQuery/jquery.trackbar.js"></script>
</head>
<body>
<header>
    <?php

    include("/header.php");

    ?>
</header>

<div id="content-cart">
    <?php
    $action = clear_string($_GET["action"]);
    switch ($action) {
        case 'oneclick':
            echo '
					<div id="block-step">
						<div id="name-step">
							<ul>
								<li><a id="active-li">1. Корзина товаров</a></li>
								<li><span>&rarr;</span></li>
								<li><a>2. Контактная информация</a></li>
								<li><span>&rarr;</span></li>
								<li><a>3. Завершение</a></li>
							</ul>
						</div>
							<p>шаг 1 из 3</p>
					</div>

				';

            $result = mysqli_query($connection, "SELECT * FROM `cart`, `product` WHERE product.ID = cart.cart_id_product ");
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_array($result);
                echo '
					<div id="header-list-cart">
					<div id="head1">Изображение</div>
					<div id="head2">Наименование товара</div>
					<div id="head3">Количество</div>
					<div id="head4">Цена</div>
					</div>
				';
                do {
                    if (strlen($row["IMAGE"]) > 0 && file_exists("upload_images/".$row["IMAGE"])) {
                        $img_path = 'upload_images/'.$row["IMAGE"];
                        $max_width = 100;
                        $max_height = 100;
                        list($width, $height) = getimagesize($img_path);
                        $ratioh = $max_height / $height;
                        $ratiow = $max_width / $width;
                        $ratio = min($ratioh, $ratiow);
                        $width = intval($ratio*$width);
                        $height = intval($ratio*$height);
                    } else {
                        $img_path = "images/noimages.jpg";
                        $width = 120;
                        $height = 105;
                    }
                    echo '
				<div id="block-list-cart">
					<div class="img-cart">
						<p align="center"><img src=" '.$img_path.' " width=" '.$width.' " height=" '.$height.'" /></p>
					</div>
				<div id="title-cart">
					<p align="center">'.$row["NAME"].'</p>
				</div>
					<div id="count-cart">
						<ul id="input-count-style">
							<li>
								<p align="center">'.$row["count_products"].' шт</p>
							</li>
						</ul>
					</div>
				<div id="price-product"><h5><span class="span-count">'.$row["count_products"].'</span> x <span>'.$row["PRICE"].' руб.</span></h5><h3><p class="p-price">'.$row["PRICE"]*$row["count_products"].' руб</p></h3></div>
				<div id="delete-cart"><a href="cart.php?id='.$row["cart_id"].'&action=delete"><img src="/images/Delete_40.png"></a></div>
</div>
				';
                } while ($row = mysqli_fetch_array($result));
                echo '
		<h2 id="itog-price">Итого: <strong>'.$all_price.'</strong> рублей</h2>
		<a href="cart.php?action=clear" id="button-next">Очистить</a>
		<a href="cart.php?action=confirm" id="button-next">Далее</a>
	';
            } else {
                echo '<h3 id="clear-cart">Корзина пуста</h3> ';
            }

            break;
        case 'confirm':
            echo '
					<div id="block-step">
						<div id="name-step">
							<ul>
							<li><a href="cart.php?action=oneclick">1. Корзина товаров</a></li>
							<li><span>&rarr;</span></li>
							<li><a id="active-li">2. Контактная информация</a></li>
							<li><span>&rarr;</span></li>
							<li><a>3. Завершение</a></li>
							</ul>
						</div>
						<p>шаг 2 из 3</p>
					</div>
				';

            $chck = "";
            if ($_SESSION["order_delivery"] == "По почте") {
                $chck1 = "checked";
            }
            if ($_SESSION["order_delivery"] == "Курьером") {
                $chck2 = "checked";
            }
            if ($_SESSION["order_delivery"] == "Самовывоз") {
                $chck3 = "checked";
            }

            echo '
				<h3 id="title-h3">Способы доставки:</h3>
				<form method="post">
				<ul id="info-radio">
				<li>
				<input type="radio" name="order_delivery" class="order_delivery" id="order_delivery1" required value="По почте">
				<label class="label-delivary" for="order_delivery1"> По почте</label>
				</li>

				<li>
				<input type="radio" name="order_delivery" class="order_delivery" id="order_delivery2" required value="Курьером">
				<label class="label-delivary" for="order_delivery2">Курьером</label>
				</li>

				<li>
				<input type="radio" name="order_delivery" class="order_delivery" id="order_delivery3" required value="Самовывоз">
				<label class="label-delivary" for="order_delivery3">Самовывоз</label>
				</li>
				</ul>
				<h3 id="title-h3">Информация для доставки:</h3>
				<ul id="info-order">
    					<li><span class="order_span_style">ФИО</span><input type="text" name="order_fio" id="order_fio" required value="'.$_SESSION["data_fullname"].'"/><span class="order_span_style">Пример: Иванов Иван Иванович</span></li>

					<li><span class="order_span_style">Email</span><input type="email" name="order_email"  id="order_email" required value="'.$_SESSION["data_email"].'"><span class="order_span_style">Пример: ivanov@mail.ru</span></li>

				 	<li><span class="order_span_style">Телефон</span><input type="tel" name="order_phone"  id="order_phone" required pattern="/^[\d]{1}\ \([\d]{2,3}\)\ [\d]{2,3}-[\d]{2,3}-[\d]{2,3}$/" value="'.$_SESSION["data_phone"].'"><span class="order_span_style">Пример: 8 930 420 71 91</span></li>

				 	<li><span class="order_span_style">Адрес</span><input type="text" name="order_address" maxlength="5"  id="order_address" required value="'.$_SESSION["data_address"].'"/><span class="order_span_style">Пример: г.Пенза, ул.Пушкина 137</span></li>
				 	</ul>
				 	<p><input type="submit" name="submitdata" id="confirm-button-next" value="Далее"<p>
				 	</form>
				 	';


            break;
        case 'completion':
            echo '
					<div id="block-step">
						<div id="name-step">
							<ul>
							<li><a href="cart.php?action=oneclick">1. Корзина товаров</a></li>
							<li><span>&rarr;</span></li>
							<li><a href="cart.php?action=confirm">2. Контактная информация</a></li>
							<li><span>&rarr;</span></li>
							<li><a id="active-li">3. Завершение</a></li>
							</ul>
						</div>
						<p>шаг 3 из 3</p>
					</div>
				';

            echo '
				<ul id="list-info">
				<li><strong>Способ доставки: </strong>'.$_SESSION["order_delivery"].'</li>
				<li><strong>Email: </strong>'.$_SESSION["order_email"].'</li>
				<li><strong>ФИО: </strong>'.$_SESSION["order_fio"].'</li>
				<li><strong>Адрес: </strong>'.$_SESSION["order_address"].' </li>
				<li><strong>Телефон: </strong>'.$_SESSION["order_phone"].'</li>
				</ul>
				<script>alert("Письмо отправлено на вашу электронную почту");</script>
				';
            $message = "Ваш заказ принят, ожидайте звонка";
            mail("roman_petrushov201098@mail.ru", "MyMessage", $message);
            break;

        default:
            echo '
					<div id="block-step">
						<div id="name-step">
							<ul>
							<li><a id="active-li">1. Корзина товаров</a></li>
							<li><span>&rarr;</span></li>
							<li><a>2. Контактная информация</a></li>
							<li><span>&rarr;</span></li>
							<li><a>3. Завершение</a></li>
							</ul>
						</div>
						<p>шаг 1 из 3</p>
						<a href	= "cart.php?action=clear">Очистить</a>
					</div>
				';

            $result = mysqli_query($connection, "SELECT * FROM cart, `product` WHERE 1 AND product.ID = cart.cart_id_product");
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_array($result);
                echo '
 <div id="header-list-cart">
 <div id="head1">Изображение</div>
 <div id="head2">Наименование товара</div>
 <div id="head3">Количество</div>
 <div id="head4">Цена</div>
 </div>
';
                do {
                    if (strlen($row["IMAGE"]) > 0 && file_exists("upload_images/".$row["IMAGE"])) {
                        $img_path = 'upload_images/'.$row["IMAGE"];
                        $max_width = 100;
                        $max_height = 100;
                        list($width, $height) = getimagesize($img_path);
                        $ratioh = $max_height / $height;
                        $ratiow = $max_width / $width;
                        $ratio = min($ratioh, $ratiow);
                        $width = intval($ratio*$width);
                        $height = intval($ratio*$height);
                    } else {
                        $img_path = "images/noimages.jpg";
                        $width = 120;
                        $height = 105;
                    }
                    echo '
<div id="block-list-cart">
 <div class="img-cart">
  <p align="center"><img src=" '.$img_path.' " width=" '.$width.' " height=" '.$height.'" /></p>
 </div>
<div id="title-cart">
 <p align="center">'.$row["NAME"].'</p>
</div>
 <div id="count-cart">
  <ul id="input-count-style">
   <li>
    <p align="center">'.$row["count_products"].' шт</p>
   </li>
  </ul>
 </div>
<div id="price-product"><h5><span class="span-count">'.$row["count_products"].'</span> x <span>'.$row["PRICE"].' руб.</span></h5><h3><p class="p-price">'.$row["PRICE"]*$row["count_products"].' руб</p></h3></div>
<div id="delete-cart"><a href="cart.php?id='.$row["cart_id"].'&action=delete"><img src="/images/Delete_40.png"></a></div>
</div>
';
                } while ($row = mysqli_fetch_array($result));
                echo '
<h2 id="itog-price">Итого: <strong>'.$all_price.'</strong> рублей</h2>
<a href="cart.php?action=confirm" id="button-next">Далее</a>
';
            } else {
                echo '<h3 id="clear-cart">Корзина пуста</h3> ';
            }
            break;
    }

    ?>
</div>

<script src="/js/jquery-3.1.1.min.js"></script>
<script src="/js/bucket.js"></script>
<script src="/js/shop-script.js"></script>

</body>
</html>
jQuery(document).ready(function($) {
	var product = $("#tovar");
	$(".add-cart-style-list").on('click', function(event) {
		console.log("id " + this.dataset.idWheel + " count " + $(this).prev().find('.input-count').val());
		$.ajax({
			url: "include/addtocart.php?id=" + this.dataset.idWheel + "?count-tovar=" + $(this).prev().find('.input-count').val(),
			type: 'GET'
		})
		.done(function(data) {
			$("#tovar1").html(data);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});
});
<?php
session_start();
if($_SESSION['auth_admin'] == "yes_auth"){
    if (isset ($_GET["logout"]))
    {
        unset($_SESSION['auth_admin']);
        header("Location: login.php");
    }

    $_SESSION['urlpage'] = '<a href="index.php">Главная</a>\<a href="shoes.php">Одежда</a>';

    include("../include/db_connect.php");

    $action = $_GET["action"];
    if (isset($action)) {
        $id = (int)$_GET["id"];

        switch ($action) {
            case 'delete':
                $delete = mysqli_query($connection,"DELETE FROM shoes WHERE ID='$id'");
                break;
        }
    }
    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Панель управления</title>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="jquery_confirm/jquery_confirm.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    </head>
    <body>
    <div id="block-body">
        <div id="block-header">
            <div id="block-header1">
                <h3>Магазин "Гараж" - Панель Управления</h3>
                <p id="link-nav"><?php echo  $_SESSION['urlpage']; ?></p>
            </div>
            <div id="block-header2">
                <p align="right"><a href="administrators.php">Администраторы</a>| <a href="?logout">Выход</a></p>
                <p align="right">Вы - <span>Администратор</span></p>
            </div>
        </div>

        <div id="left-nav">
            <ul>
                <li><a href="index.php">Панель управления</a></li>
                <li><a href="users.php">Пользователи</a></li>
                <li><a href="tovar.php">Оформленные товары</a></li>
                <li><a href="clothes.php">Одежда</a></li>
                <li><a href="accessories.php">Аксессуары</a></li>
            </ul>
            <?php
            $all_count = mysqli_query($connection, "SELECT * FROM shoes");
            $all_count_result = mysqli_num_rows($all_count);
            ?>
        </div>

        <div id="block-content">
            <div id="block-parametrs">

            </div>
            <div id="block-info">
                <p id="count-style">Всего обуви - <strong><?php echo $all_count_result; ?></strong></p>
                <p align="right" id="add-style"><a href="add_shoes.php">Добавить обувь</a></p>
            </div>

            <ul id="block-tovar">
                <?php
                $num = 9;
                $page = (int)$_GET['page'];

                $count = mysqli_query($connection, "SELECT COUNT(*) FROM shoes");
                $temp = mysqli_fetch_array($count);
                $post = $temp[0];

                //Находим общее число страниц
                $total = (($post - 1) / $num) + 1;
                $total = intval($total);

                //Определим начало сообщений для текущей страницы
                $page = intval($page);
                //Если значение $page меньше единицы или отрицательно
                //переходим на первую страницы
                //А если слишком большое, то переходим на последнюю
                if (empty($page) or $page < 0) $page = 1;
                if ($page > $total) $page = $total;
                //Вычисляем начиная с какого номера
                //следует выводить сообщения
                $start = $page * $num - $num;

                if ($temp[0] > 0) {
                    $result = mysqli_query($connection, "SELECT * FROM shoes ORDER BY ID DESC LIMIT $start, $num");
                    if (mysqli_num_rows($result) > 0) {
                        $row = mysqli_fetch_array($result);
                        do{
                            if(strlen($row["IMAGE"]) > 0 && file_exists("../upload_images/".$row["IMAGE"])){
                                $img_path = '../upload_images/'.$row["IMAGE"];
                                $max_width = 100;
                                $max_height = 100;
                                list($width, $height) = getimagesize($img_path);
                                $ratioh = $max_height / $height;
                                $ratiow = $max_width / $width;
                                $ratio = min($ratioh, $ratiow);
                                $width = intval($ratio*$width);
                                $height = intval($ratio*$height);
                            }
                            else{
                                $img_path = "../images/noimages.jpg";
                                $width = 120;
                                $height = 105;
                            }

                            echo '
                           <li>
                              <p>'.$row['NAME'].'</p>
                              <center>
                                <img src="'.$img_path.'" width="'.$width.'" height="'.$height.'" />
                              </center>
                              <p align="center" id="link_action">
                               <a class="green" href="edit_shoes.php?id='.$row["ID"].'">Изменить</a> | <a rel="shoes.php?id='.$row["ID"].'&action=delete" class="delete">Удалить</a>
                              </p>
                           </li>
                       ';
                        }
                        while ($row = mysqli_fetch_array($result));
                        echo '
                         </ul>
                     ';
                    }
                }

                if ($page != 1) $pervpage = '<li><a class="pstr-prev" href=?page='. ($page - 1) .' >Назад</a></li>';
                if ($page != $total) $nextpage = '<li><a class="pstr-next" href=?page='. ($page + 1) .' >Вперед</a></li>';

                //Формируем ссылки со страницами
                if($page - 5 > 0) $page5left = '<li><a href=shoes.php?page='. ($page - 5) .'>'. ($page - 5) .'</a></li>';
                if($page - 4 > 0) $page4left = '<li><a href=shoes.php?page='. ($page - 4) .'>'. ($page - 4) .'</a></li>';
                if($page - 3 > 0) $page3left = '<li><a href=shoes.php?page='. ($page - 3) .'>'. ($page - 3) .'</a></li>';
                if($page - 2 > 0) $page2left = '<li><a href=shoes.php?page='. ($page - 2) .'>'. ($page - 2) .'</a></li>';
                if($page - 1 > 0) $page1left = '<li><a href=shoes.php?page='. ($page - 1) .'>'. ($page - 1) .'</a></li>';

                if($page + 5 <= $total) $page5right = '<li><a href=shoes.php?page='. ($page + 5) .'>'. ($page + 5) .'</a></li>';
                if($page + 4 <= $total) $page4right = '<li><a href=shoes.php?page='. ($page + 4) .'>'. ($page + 4) .'</a></li>';
                if($page + 3 <= $total) $page3right = '<li><a href=shoes.php?page='. ($page + 3) .'>'. ($page + 3) .'</a></li>';
                if($page + 2 <= $total) $page2right = '<li><a href=shoes.php?page='. ($page + 2) .'>'. ($page + 2) .'</a></li>';
                if($page + 1 <= $total) $page1right = '<li><a href=shoes.php?page='. ($page + 1) .'>'. ($page + 1) .'</a></li>';



                ?>
                <div id="footerfix">
                    <?php
                    if ($total > 1) {
                        echo '
                     <center>
                     <div class="pstrnav">
                     <ul>
                     ';
                        echo $pervpage.$page4left.$page3left.$page3left.$page2left.$page1left."<li><a class='pstr-active' href='shoes.php?page=".$page."'>".$page."</a></li>".$page1right.$page2right.$page3right.$page4right.$page5right.$nextpage;
                        echo '
              </ul>
              </div>
              </center>
              ';
                    }
                    ?>
                </div>
        </div>

    </div>

    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="jquery_confirm/jquery_confirm.js"></script>
    </body>
    </html>
    <?php
}else {
    header("Location: login.php");
}
?>

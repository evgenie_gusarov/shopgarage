<?php
session_start();
if ($_SESSION['auth_admin'] == "yes_auth") {
    if (isset($_GET["logout"])) {
        unset($_SESSION['auth_admin']);
        header("Location: login.php");
    }

    $_SESSION['urlpage'] = '<a href="index.php">Главная</a> \ <a href="shoes.php"> Обувь </a>\<a> Добавление обуви </a>';

    include("../include/db_connect.php");

    if ($_POST['submit_add']) {
        $error = array();

        if (!$_POST["form_title"]) {
            $error[] = "Укажите название обуви";
        }

        if (!$_POST["form_type"]) {
            $error[] = "Укажите тип обуви";
        }


        if (!$_POST["form_size"]) {
            $error[] = "Укажите размер обуви";
        }

        if (!$_POST["form_description"]){
            $error[] = "Укажите описание обуви";
        }

        if (!$_POST["form_price"]) {
            $error[] = "Укажите цену обуви";
        }

        if (!$_POST["form_brand"]) {
            $error[] = "Укажите бренд обуви";
        }

        if (count($error)) {
            $_SESSION['message'] = "<p id='form-error'>".implode('<br />', $error)."</p>";
        } else {
            $result = mysqli_query($connection, "INSERT INTO shoes(TYPE, NAME, SIZE, PRICE, BRAND, DESCRIPTION)
       VALUES(
         '".$_POST["form_type"]."',
         '".$_POST["form_title"]."',
         '".$_POST["form_size"]."',
         '".$_POST["form_price"]."',
         '".$_POST["form_brand"]."',
         '".$_POST["form_description"]."')");


            $_SESSION['message'] = "<p id='form-success'>Обувь успешно добавлена</p>";
            $id = mysqli_insert_id($connection);

            if (empty($_POST["upload_image"])) {
                include("/action/upload_image_shoes.php");
                unset($_POST["upload_image"]);
            }
        }
    } ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Панель управления</title>
        <link rel="stylesheet" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    </head>
    <body>
    <div id="block-body">
        <div id="block-header">
            <div id="block-header1">
                <h3>Магазин "Гараж" - Панель Управления</h3>
                <p id="link-nav"><?php echo  $_SESSION['urlpage']; ?></p>
            </div>
            <div id="block-header2">
                <p align="right"><a href="administrators.php">Администраторы</a>| <a href="?logout">Выход</a></p>
                <p align="right">Вы - <span>Администратор</span></p>
            </div>
        </div>

        <div id="left-nav">
            <ul>
                <li><a href="index.php">Панель управления</a></li>
                <li><a href="users.php">Пользователи</a></li>
                <li><a href="tovar.php">Оформленные товары</a></li>
                <li><a href="clothes.php">Одежда</a></li>
                <li><a href="accessories.php">Аксессуары</a></li>
            </ul>
        </div>

        <div id="block-content">
            <div id="block-parametrs">
                <p id="title-page">Добавление обуви</p>
            </div>
            <?php
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            } ?>
            <form enctype="multipart/form-data" method="post">
                <ul id="edit-tovar">
                    <li>
                        <label>Название обуви</label>
                        <input type="text" name="form_title">
                    </li>

                    <li>
                        <label>Тип обуви</label>
                        <select id="form_type" name="form_type">
                            <option value=""></option>
                            <option value="Берцы">Берцы</option>
                            <option value="Босоножки">Босоножки</option>
                            <option value="Ботфорты">Ботфорты</option>
                            <option value="Вибрамы">Вибрамы</option>
                            <option value="Ворк бутс">Ворк бутс</option>
                            <option value="Гриндерсы">Гриндерсы</option>
                            <option value="Кеды">Кеды</option>
                            <option value="Кроксы">Кроксы</option>
                            <option value="Кроссовки">Кроссовки</option>
                            <option value="Лабутены">Лабутены</option>
                            <option value="Пантолеты">Пантолеты</option>
                            <option value="Слипоны">Слипоны</option>
                            <option value="Сникеры">Сникеры</option>
                            <option value="Тимберленды">Тимберленды</option>
                            <option value="Угги">Угги</option>
                            <option value="Чешки">Чешки</option>
                        </select>
                    </li>

                    <li>
                        <label>Размер обуви</label>
                        <select id="form_type" name="form_size">
                            <option value=""></option>
                            <option value="34.5">34.5</option>
                            <option value="35">35</option>
                            <option value="35.5">35.5</option>
                            <option value="36">36</option>
                            <option value="36.5">36.5</option>
                            <option value="37">37</option>
                            <option value="37.5">37.5</option>
                            <option value="38">38</option>
                            <option value="38.5">38.5</option>
                            <option value="39">39</option>
                            <option value="39.5">39.5</option>
                            <option value="40">40</option>
                            <option value="40.5">40.5</option>
                            <option value="41">41</option>
                            <option value="41.5">41.5</option>
                            <option value="42">42</option>
                            <option value="42.5">42.5</option>
                            <option value="43">43</option>
                            <option value="43.5">43.5</option>
                            <option value="44">44</option>
                            <option value="44.5">44.5</option>
                            <option value="45">45</option>
                            <option value="45.5">45.5</option>
                            <option value="46">46</option>
                            <option value="46.5">46.5</option>
                            <option value="47">47</option>
                            <option value="48">48</option>
                            <option value="49">49</option>
                        </select>
                    </li>

                    <li>
                        <label>Бренд обуви</label>
                        <input type="text" name="form_brand">
                    </li>

                    <li>
                        <label>Описание обуви</label>
                        <input type="text" name="form_description">
                    </li>

                    <li>
                        <label>Цена обуви</label>
                        <input type="text" name="form_price">
                    </li>

                </ul>

                <label class="stylelabel">Картинка</label>
                <div id="baseimg-upload">
                    <input type="hidden" name="MAX_FILE_SIZE" value="5000000">
                    <input type="file" name="upload_image" value="">
                </div>
                <p align="right"><input type="submit" name="submit_add" id="submit_form" value="Добавить обувь"></p>
            </form>
        </div>
    </div>
    </body>
    </html>
    <?php

} else {
    header("Location: login.php");
}
?>



<?php
 session_start();
 if ($_SESSION['auth_admin'] == "yes_auth") {
     if (isset($_GET["logout"])) {
         unset($_SESSION['auth_admin']);
         header("Location: login.php");
     }
     $_SESSION['urlpage'] = '<a href="index.php">Главная</a>';
     include("../include/db_connect.php"); ?>
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>Панель управления</title>
  <link rel="stylesheet" href="css/style.css">
 </head>
 <body>
    <div id="block-body">
      <div id="block-header">
        <div id="block-header1">
          <h3>Магазин "Гараж" - Панель Управления</h3>
            <p id="link-nav"><?php echo  $_SESSION['urlpage']; ?></p>
         </div>
            <div id="block-header2">
              <p align="right"><a href="administrators.php">Администраторы</a>| <a href="?logout">Выход</a></p>
              <p align="right">Вы - <span>Администратор</span></p>
            </div>
      </div>

        <div id="left-nav">
          <ul>
              <li><a href="users.php">Пользователи</a></li>
              <li><a href="tovar.php">Оформленные товары</a></li>
              <li><a href="clothes.php">Одежда</a></li>
              <li><a href="shoes.php">Обувь</a></li>
              <li><a href="accessories.php">Аксессуары</a></li>
          </ul>
        </div>

         <div id="block-content">
           <div id="block-parametrs">
             <p id="title-page">Панель управления</p>
           </div>
         </div>
   </div>
 </body>
</html>
<?php

 } else {
     header("Location: login.php");
 }
 ?>

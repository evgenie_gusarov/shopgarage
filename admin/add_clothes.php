<?php
  session_start();
   if ($_SESSION['auth_admin'] == "yes_auth") {
       if (isset($_GET["logout"])) {
           unset($_SESSION['auth_admin']);
           header("Location: login.php");
       }

       $_SESSION['urlpage'] = '<a href="index.php">Главная</a> \ <a href="clothes.php"> Одежда </a>\<a> Добавление одежды </a>';

       include("../include/db_connect.php");

       if ($_POST['submit_add']) {
           $error = array();

           if (!$_POST["form_title"]) {
               $error[] = "Укажите название одежды";
           }

           if (!$_POST["form_type"]) {
               $error[] = "Укажите тип одежды";
           }


           if (!$_POST["form_size"]) {
               $error[] = "Укажите размер одежды";
           }

           if (!$_POST["form_description"]){
               $error[] = "Укажите описание одежды";
           }

           if (!$_POST["form_price"]) {
               $error[] = "Укажите цену одежды";
           }

           if (!$_POST["form_brand"]) {
               $error[] = "Укажите бренд одежды";
           }

           if (count($error)) {
               $_SESSION['message'] = "<p id='form-error'>".implode('<br />', $error)."</p>";
           } else {
               $result = mysqli_query($connection, "INSERT INTO clothes(TYPE, NAME, SIZE, PRICE, BRAND, DESCRIPTION)
       VALUES(
         '".$_POST["form_type"]."',
         '".$_POST["form_title"]."',
         '".$_POST["form_size"]."',
         '".$_POST["form_price"]."',
         '".$_POST["form_brand"]."',
         '".$_POST["form_description"]."')");


               $_SESSION['message'] = "<p id='form-success'>Одежда успешно добавлена</p>";
               $id = mysqli_insert_id($connection);

               if (empty($_POST["upload_image"])) {
                   include("/action/upload_image_clothes.php");
                   unset($_POST["upload_image"]);
               }
           }
       } ?>
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
    <title>Панель управления</title>
      <link rel="stylesheet" href="css/style.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
 </head>
 <body>
    <div id="block-body">
      <div id="block-header">
        <div id="block-header1">
          <h3>Магазин "Гараж" - Панель Управления</h3>
            <p id="link-nav"><?php echo  $_SESSION['urlpage']; ?></p>
         </div>
            <div id="block-header2">
              <p align="right"><a href="administrators.php">Администраторы</a>| <a href="?logout">Выход</a></p>
              <p align="right">Вы - <span>Администратор</span></p>
            </div>
      </div>

        <div id="left-nav">
          <ul>
              <li><a href="index.php">Панель управления</a></li>
              <li><a href="users.php">Пользователи</a></li>
              <li><a href="tovar.php">Оформленные товары</a></li>
              <li><a href="shoes.php">Обувь</a></li>
              <li><a href="accessories.php">Аксессуары</a></li>
          </ul>
        </div>

         <div id="block-content">
           <div id="block-parametrs">
             <p id="title-page">Добавление одежды</p>
           </div>
           <?php
           if (isset($_SESSION['message'])) {
               echo $_SESSION['message'];
               unset($_SESSION['message']);
           } ?>
            <form enctype="multipart/form-data" method="post">
             <ul id="edit-tovar">
               <li>
                 <label>Название одежды</label>
                 <input type="text" name="form_title">
               </li>

               <li>
                 <label>Тип одежды</label>
                 <select id="form_type" name="form_type">
                     <option value=""></option>
                     <option value="Куртка">Куртка</option>
                     <option value="Ветровка">Ветровка</option>
                     <option value="Кофта">Кофта</option>
                     <option value="Майка">Майка</option>
                     <option value="Джинсы">Джинсы</option>
                     <option value="Шорты">Шорты</option>
                     <option value="Бриджи">Бриджи</option>
                     <option value="Брюки">Брюки</option>
                </select>
               </li>

               <li>
                 <label>Размер одежды</label>
                   <select id="form_type" name="form_size">
                       <option value=""></option>
                       <option value="46-48 S ( мужской )">46-48 - S ( мужской )</option>
                       <option value="48-50 M ( мужской )">48-50 - M ( мужской )</option>
                       <option value="50-52 L ( мужской )">50-52 - L ( мужской )</option>
                       <option value="52-54 XL ( мужской )">52-54 - XL ( мужской )</option>
                       <option value="42-44 XS ( женский )">42-44 - XS ( женский )</option>
                       <option value="44-46 S ( женский )">44-46 - S ( женский )</option>
                       <option value="46-48 M ( женский )">46-48 - M ( женский )</option>
                       <option value="48-50 L ( женский )">48-50 - L ( женский )</option>
                       <option value="50-52 XL ( женский )">50-52 - XL ( женский )</option>
                   </select>
               </li>

               <li>
                 <label>Бренд одежды</label>
                 <input type="text" name="form_brand">
               </li>

               <li>
                   <label>Описание одежды</label>
                   <input type="text" name="form_description">
               </li>

               <li>
                 <label>Цена одежды</label>
                 <input type="text" name="form_price">
               </li>

             </ul>

              <label class="stylelabel">Картинка</label>
               <div id="baseimg-upload">
                 <input type="hidden" name="MAX_FILE_SIZE" value="5000000">
                 <input type="file" name="upload_image" value="">
               </div>
                <p align="right"><input type="submit" name="submit_add" id="submit_form" value="Добавить одежду"></p>
            </form>
         </div>
   </div>
 </body>
</html>
<?php

   } else {
       header("Location: login.php");
   }
 ?>

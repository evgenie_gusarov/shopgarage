<?php
require "/bd.php";
include("include/db_connect.php");
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Интернет-магазин "Колеса.ру"</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="/fonts/fonts.css">
</head>
<body>

	<header>
	<?php
            include("/header.php");
     ?>
</header>

		<div id="content_center">
			<h3 id = "title_oplata">Шиномонтаж в Пензе</h3>
			<p id="payment_methods">Цены на полный комплекс шиномонтажных работ на 4 колеса</p>
			<table>
			<tbody>
				<tr style="height: 50px">
					<td><b>Легковые:</b></td>
				</tr>
				<tr>
					<td>Полный комплекс 13 - </td>
					<td>900 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 14 - </td>
					<td>1200 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 15 - </td>
					<td>1500 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 16 - </td>
					<td >1600 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 17 - </td>
					<td>1700 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 18 - </td>
					<td>1800 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 19 - </td>
					<td>1900 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 20 - </td>
					<td>2100 руб.</td>
				</tr>

				<tr style="height: 50px">
					<td><b>Внедорожники:</b></td>
				</tr>
				<tr>
					<td>Полный комплекс 15 - </td>
					<td>1700 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 16 - </td>
					<td>1900 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 17 - </td>
					<td>2100 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 18 - </td>
					<td>2300 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 19 - </td>
					<td>2500 руб.</td>
				</tr>
				<tr>
					<td>Полный комплекс 20 - </td>
					<td>2700 руб.</td>
				</tr>
			</tbody>
			</table>
		</div>


</body>
</html>

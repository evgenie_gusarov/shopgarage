<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</script>
<script src="js/jquery.min.js"></script>

<!----Начало анимации--->
<script type="text/javascript">
    var $ = jQuery.noConflict();
    $(function() {
        $('#activator').click(function(){
            $('#box').animate({'top':'0px'},500);
        });
        $('#boxclose').click(function(){
            $('#box').animate({'top':'-700px'},500);
        });
    });
    $(document).ready(function(){

        //Hide (Collapse) the toggle containers on load
        $(".toggle_container").hide();

        // Открытие и закрытие блока меню по кнопке
        $(".trigger").click(function(){
            $(this).toggleClass("active").next().slideToggle("slow");
            return false; //Prevent the browser jump to the link anchor
        });

    });
</script>

<!-- Шапка магазина -->
<div class="header">
    <div class="wrap">

        <div class = "col-30">

            <a href="/index.php"><img id="logo" src="/images/logo.png" alt=""></a>
            <p id="logo-name">Сеть магазинов</p>

        </div>

        <div class="col-50">

            <div id="size_products">

                <?php
                $result = mysqli_query($connection, "SELECT * FROM cart, clothes WHERE 1 AND clothes.ID = cart.cart_id_product");
                if (mysqli_num_rows($result) > 0) {
                    $row = mysqli_fetch_array($result);
                    do {
                        $count = $count + $row["count_products"];
                        $int =$row["PRICE"] * $row["count_products"];
                        $all_price = $all_price + $int;
                    } while ($row = mysqli_fetch_array($result));
                    echo '<p id="block-basket"><a href="cart.php?action=oneclick"><img id="img-cart1"src="/images/Shopping Cart Loaded-50.png"></a><span id="tovar1">Товаров: '.$count.' шт.<br>На сумму: '.$all_price.' рублей</span></p>';
                } else {
                    echo '<p id="block-basket"><a href="cart.php?action=oneclick"><img id="img-cart"src="/images/Shopping Cart Loaded-50.png"></a><span id="tovar1">Корзина пуста</span></p>';
                }
                ?>

            </div>

            <div class="top-searchbar">

                <form>
                    <input type="text" /><input type="submit" value="" />
                </form>

            </div>



        </div>

        <div class="col-25">

            <div id="reg_avtor">

                <?php
                session_start();
                if (isset($_SESSION['logged_user'])) : ?>

                    <div id="avatar">

                        <ul id="main-menu">
                            <li><a id="profile_data"><?php echo $_SESSION['logged_user']->login;?></a>
                                <ul id="sub-menu">
                                    <li><a href="data_profile.php">Данные</a></li>
                                    <li><a href="profile.php">Изменить профиль</a></li>
                                    <li><a href="/logout.php">Выйти</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>



                <?php else :   ?>
                    <a id="turquoise" href="/login.php">Авторизация</a><br />
                    <a id="turquoisereg" href="/reg.php">Регистрация</a>
                <?php endif; ?>

            </div>

        </div>

    </div>

    <div class="menu">
        <nav>
            <ul>
                <li>
                    <a href="#" class="right_bt" id="activator">Разделы</a>
                </li>

                <li>
                    <a href="/delivery.php">доставка</a>
                </li>

                <li>
                    <a href="/PointsOfIssue.php">Расположение магазина</a>
                </li>

                <li>
                    <a href="/place_order.php">Оформить заказ</a>
                </li>
            </ul>
        </nav>
        <!--выплывающее меню-->
        <div class="box" id="box">

            <div class="box_content">

                <div class="box_content_center">

                    <div class="form_content">

                        <div class="menu_box_list">

                            <ul>
                                <li><a href="/product_clothes.php"><span>Одежда</span></a></li>
                                <li><a href="/product_shoes.php"><span>Обувь</span></a></li>
                                <li><a href="/product_accecssoreis.php"><span>Аксессуары</span></a></li>

                                <div class="clear"> </div>

                            </ul>

                        </div>

                        <a class="boxclose" id="boxclose"> <span> </span></a>

                    </div>

                </div>

            </div>

        </div>
        <!--------------->
    </div>
</div>
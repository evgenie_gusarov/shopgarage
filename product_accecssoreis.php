<?php
require "/bd.php";
include("include/db_connect.php");
session_start();

$sorting = $_GET ["sort"];
switch ($sorting) {
    case 'price-asc':
        $sort = 'PRICE ASC';
        $sorting = 'price-asc';
        $sort_name = 'От дешевых к дорогим';
        break;

    case 'price-desc':
        $sort = 'PRICE DESC';
        $sorting = 'price-desc';
        $sort_name = 'От дорогих к дешевым';
        break;

    case 'no-sort':
        $sort = 'ID ASC';
        $sorting = 'price-desc';
        $sort_name = 'Нет сортировки';
        break;

    default:
        $sort = 'ID ASC';
        $sorting = '';
        $sort_name = 'Нет сортировки';
        break;
}
?>
!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Kolesa</title>
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/product_section.css">
    <link rel="stylesheet" href="/fonts/fonts.css">
    <script type="text/javascript" src="trackbar/jQuery/jquery-1.2.3.min.js"></script>
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="trackbar/jQuery/jquery.trackbar.js"></script>
</head>
<body>
<header>
    <?php
    include("/header.php");
    ?>
</header>
<div id="content_header_center">
    <div>
        <?php
        include("/filter.php");
        ?>
    </div>
    <div id="input__tovar">
        <?php
        $num = 32;
        $page = (int)$_GET['page'];

        $count = mysqli_query($connection, "SELECT COUNT(*) FROM `accessories` ");
        $temp = mysqli_fetch_array($count);
        $post = $temp[0];

        //Находим общее число страниц
        $total = (($post - 1) / $num) + 1;
        $total = intval($total);

        //Определим начало сообщений для текущей страницы
        $page = intval($page);
        //Если значение $page меньше единицы или отрицательно
        //переходим на первую страницы
        //А если слишком большое, то переходим на последнюю
        if (empty($page) or $page < 0) {
            $page = 1;
        }
        if ($page > $total) {
            $page = $total;
        }
        //Вычисляем начиная с какого номера
        //следует выводить сообщения
        $start = $page * $num - $num;
        $qury_start = "LIMIT $start, $num";


        if ($temp[0] > 0) {
            $result = mysqli_query($connection, "SELECT * FROM `accessories` WHERE 1 ORDER BY $sort $qury_start");
            if (mysqli_num_rows($result) > 0) {
                $rows = mysqli_fetch_array($result);
                do {
                    echo '
                          <div id="label">
                            <div id="picture">
                              <img src="/upload_images/'.$rows["IMAGE"].'"/>
                            </div>
                              <div id="title">
                                <p id="size"></p>
                              </div>

                                <div>
              						 <div id="product-price"><strong>'.$rows["NAME"].':'.$rows["PRICE"].' ₽</strong></div>
              					</div>
              					<div id="delivary">Кол-во: <input class="input-count" type="text" name="count_tovar" value="1"><span> шт.</span></div>
                                <a class="add-cart-style-list" data-id-wheel="'. $rows["ID"] .'" ></a>
                          </div>

                      ';
                } while ($rows = mysqli_fetch_array($result));
            }
        }
        if ($page != 1) {
            $pervpage = '<li><a class="pstr-prev" href=product_accecssoreis.php?page='. ($page - 1) .'&sort='.$sorting.'>Назад</a></li>';
        }
        if ($page != $total) {
            $nextpage = '<li><a class="pstr-next" href=product_accecssoreis.php?page='. ($page + 1) .'&sort='.$sorting.' >Вперед</a></li>';
        }

        //Формируем ссылки со страницами
        if ($page - 5 > 0) {
            $page5left = '<li><a href=product_accecssoreis.php?page='. ($page - 5) .'&sort='.$sorting.'>'. ($page - 5) .'</a></li>';
        }
        if ($page - 4 > 0) {
            $page4left = '<li><a href=product_accecssoreis.php?page='. ($page - 4) .'&sort='.$sorting.'>'. ($page - 4) .'</a></li>';
        }
        if ($page - 3 > 0) {
            $page3left = '<li><a href=product_accecssoreis.php?page='. ($page - 3) .'&sort='.$sorting.'>'. ($page - 3) .'</a></li>';
        }
        if ($page - 2 > 0) {
            $page2left = '<li><a href=product_accecssoreis.php?page='. ($page - 2) .'&sort='.$sorting.'>'. ($page - 2) .'</a></li>';
        }
        if ($page - 1 > 0) {
            $page1left = '<li><a href=product_accecssoreis.php?page='. ($page - 1) .'&sort='.$sorting.'>'. ($page - 1) .'</a></li>';
        }

        if ($page + 5 <= $total) {
            $page5right = '<li><a href=product_accecssoreis.php?page='. ($page + 5) .'&sort='.$sorting.'>'. ($page + 5) .'</a></li>';
        }
        if ($page + 4 <= $total) {
            $page4right = '<li><a href=product_accecssoreis.php?page='. ($page + 4) .'&sort='.$sorting.'>'. ($page + 4) .'</a></li>';
        }
        if ($page + 3 <= $total) {
            $page3right = '<li><a href=product_accecssoreis.php?page='. ($page + 3) .'&sort='.$sorting.'>'. ($page + 3) .'</a></li>';
        }
        if ($page + 2 <= $total) {
            $page2right = '<li><a href=product_accecssoreis.php?page='. ($page + 2) .'&sort='.$sorting.'>'. ($page + 2) .'</a></li>';
        }
        if ($page + 1 <= $total) {
            $page1right = '<li><a href=product_accecssoreis.php?page='. ($page + 1) .'&sort='.$sorting.'>'. ($page + 1) .'</a></li>';
        }

        ?>

        <div id="footerfix">
            <?php
            if ($total > 1) {
                echo '
                       <div id="pstrnav">
                       <ul>
                       ';
                echo $pervpage.$page4left.$page3left.$page3left.$page2left.$page1left."<li><a class='pstr-active' href=product_accecssoreis.php?page=".$page."&sort='".$sorting."'>".$page."</a></li>".$page1right.$page2right.$page3right.$page4right.$page5right.$nextpage;
                echo '
                       </ul>
                       </div>
                       ';
            }
            ?>
        </div>
    </div>
</div>
<script src="/js/jquery-3.1.1.min.js"></script>
<script src="/js/bucket.js"></script>
</body>
</html>
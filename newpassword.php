<?php
require("/bd.php");
include("include/db_connect.php");

$data = $_POST;

if (isset($data['do_reg'])) {
    $errors = array();
    if (trim($data['login']) == '') {
        $errors[]="Введите логин!";
    }
    $password = $_POST['password'];
    $user = R::findOne('users', 'login = ?', array($data['login']));
    if ($user) {
        $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
        R::store($user);
        echo '<script>alert("Вы успешно изменили пароль");</script>';
    } else {
        $errors[] = "Пользователь с таким логином не найден";
    }

    if (!empty($errors)) {
        echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
    }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>garage</title>
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/authorization_registration.css">
    <link rel="stylesheet" href="/fonts/fonts.css">
  </head>
  <body>
    <header>
        <?php
        include("/header.php")
        ?>
    </header>
    <form id="pass_recovery" action="/newpassword.php" method="POST">
        <h1>Восстановление пароля</h1>
        <fieldset id="inputs">
            <input id="username" type="text" name="login" value="<?php echo @$data['login']; ?>" placeholder="Введите ваш логин" required>
              <input id="password" name="password" type="password"  placeholder="Введите новый пароль" required>
        </fieldset>
        <fieldset id="actions">
    			<button id="submit" type="submit" name="do_reg">Сменить пароль</button>
        </fieldset>
    </form>
    </body>
    </html>

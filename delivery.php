<?php
require "/bd.php";
include("include/db_connect.php");
session_start();
?>
<!doctype html>
<html>
<head>
    <title>Интернет-магазин "Гараж.ру"</title>
    <meta charset = "utf-8">
    <link rel="stylesheet" type="text/css" href="css/store_location.css">
    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" type="text/css" href="/fonts/fonts.css">
    <script type="text/javascript" src="trackbar/jQuery/jquery-1.2.3.min.js"></script>
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="trackbar/jQuery/jquery.trackbar.js"></script>
</head>
<body>

<header>
    <?php
    include("header.php")
    ?>
</header>
<div id="content_header_center">
    <h2 id="title_oplata">Доставка</h2>
    <div>
        <ol>
            <li><b>Диапазон доставки</b></li>
            <ul id="oplata_dostavka">
                <li>Доставка осуществляется в пределах города Пенза и Пензенской.области</li>
                <li>Доставка до покупателя осуществляется не позднее чем через 48 часов после оформления заказа(при условии наличия товара на центральном складе)</li>
            </ul>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2450471.4381400957!2d42.29577535052377!3d53.145403340324!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4140e5425fb3fe87%3A0x102a3a583f197b0!2z0J_QtdC90LfQtdC90YHQutCw0Y8g0L7QsdC7Lg!5e0!3m2!1sru!2sru!4v1478334235179" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            <li><b>Стоимость доставки</b></li>
            <ul id="oplata_dostavka">
                <li>Стоимость доставки по Пензенской области — 20 руб. за 1 км. </li>
            </ul>
            <li><b>Режим доставки</b></li>
            <ul id="oplata_dostavka">
                <li>доставки осуществляются ежедневно с 11-00 до 21-00 (высокий сезон с 09-00 до 23-00 в выходные и праздники с 11-00 до 21-00)</li>
                <li>Перед выездом со склада водитель-экспедитор по телефону оповещает покупателя о своем приезде в указанный при оформлении заказа интервал времени. По желанию покупателя водитель-экспедитор может осуществить повторный звонок за 1 час или за 2 часа до приезда на адрес доставки.</li>
            </ul>
        </ol>
        <p id="time_dostavka">Ожидание водителем-экспедитором покупателя по адресу составляет не более 20 минут.</p>
    </div>
</div>
</body>
</html>

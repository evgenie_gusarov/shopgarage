<?php
require("/bd.php");
include("include/db_connect.php");
session_start();
$data = $_POST;

if (isset($data['do_login'])) {
    $user = R::findOne('users', 'login = ?', array($data['login']));
    if ($user) {
        if (password_verify($data['password'], $user->password)
        ) {
            $_SESSION['logged_user'] = $user;
            $_SESSION['user_login'] = $data['login'];
            $_SESSION['user_pass'] = $data['password'];
            ob_start();
            echo '<script>alert("Вы успешно авторизовались. Перейдите на главную страницу");</script>';
            header('Location: /index.php');
            exit;
        } else {
            $msgerror = "Неверно введен пароль";
        }
    } else {
        $msgerror = "Пользователь с таким логином не найден";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>garage</title>
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/authorization_registration.css">
    <link rel="stylesheet" href="/fonts/fonts.css">
    <script type="text/javascript" src="trackbar/jQuery/jquery-1.2.3.min.js"></script>
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="trackbar/jQuery/jquery.trackbar.js"></script>
</head>
<body>
<header>
    <?php
    include("/header.php");
    ?>
</header>

<div id="fon">
    <ul class="body_slides">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
    <form id="login" action="/login.php" method="POST">
        <?php
        if ($msgerror) {
            echo '<p id="msgerror">'.$msgerror.'</p>';
        }?>
        <p>
        <p><strong>Логин</strong></p>
        <fieldset id="inputs">
            <input id="username" type="text"  name="login" value="<?php echo @$data['login']; ?>" placeholder="Введите ваш логин">
        </fieldset>
        </p>

        <p>
        <p><strong>Пароль</strong></p>
        <fieldset id="inputs">
            <input id="password" type="password" name="password" value="<?php echo @$data['password']; ?>" placeholder="Введите ваш пароль">
        </fieldset>
        </p>
        <p>
        <fieldset id="actions">
            <a href="/newpassword.php">Забыли пароль?</a>
        </fieldset>
        </p>
        <button id="submit" type="submit" name="do_login">Войти</button>
    </form>
    <form id="vopros" action="/login.php" method="POST">
        <p><strong>У нас в первые?</strong></p>
        <fieldset id="actions">
            <a href="reg.php">Регистрация</a>
        </fieldset>
    </form>
</div>
</body>
</html>
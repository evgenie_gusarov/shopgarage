<div id="content_header_center">

    <div id="input__tovar">
        <div class="product_section" id="section1">
            <div id="name_product"><p>Одежда</p></div>
            <a href="/product_clothes.php"><div id="href_product">Ещё...</div></a>
            <?php
            $num = 3;
            $page = (int)$_GET['page'];

            $count = mysqli_query($connection, "SELECT COUNT(*) FROM `clothes` ");
            $temp = mysqli_fetch_array($count);
            $post = $temp[0];

            //Находим общее число страниц
            $total = (($post - 1) / $num) + 1;
            $total = intval($total);

            //Определим начало сообщений для текущей страницы
            $page = intval($page);
            //Если значение $page меньше единицы или отрицательно
            //переходим на первую страницы
            //А если слишком большое, то переходим на последнюю
            if (empty($page) or $page < 0) {
                $page = 1;
            }
            if ($page > $total) {
                $page = $total;
            }
            //Вычисляем начиная с какого номера
            //следует выводить сообщения
            $start = $page * $num - $num;
            $qury_start = "LIMIT $start, $num";


            if ($temp[0] > 0) {
                $result = mysqli_query($connection, "SELECT * FROM `clothes` WHERE 1 ORDER BY $sort $qury_start");
                if (mysqli_num_rows($result) > 0) {
                    $rows = mysqli_fetch_array($result);
                    do {
                        echo '
                          <div id="label">
                            <div id="picture">
                              <img id="img" src="/upload_images/'.$rows["IMAGE"].'"/>
                            </div>
                              <div id="title">
                                <p id="size"></p>
                              </div>

                                <div>
              						 <div id="product-price"><strong>'.$rows["NAME"].':'.$rows["PRICE"].' ₽</strong></div>
              					</div>
                                <div id="delivary">Кол-во: <input class="input-count" type="text" name="count_tovar" value="1"><span> шт.</span></div>
                                <a class="add-cart-style-list" data-id-wheel="'. $rows["ID"] .'" ></a>
                                
                          </div>

                      ';
                    } while ($rows = mysqli_fetch_array($result));
                }
            }
            ?>
            <div id="footerfix">
            </div>
        </div>
    </div>
    <div id="input__tovar">
        <div class="product_section" id="section2">
            <div id="name_product"><p>Обувь</p></div>
            <a href="/product_shoes.php"><div id="href_product">Ещё...</div></a>
            <?php
            $num = 3;
            $page = (int)$_GET['page'];

            $count = mysqli_query($connection, "SELECT COUNT(*) FROM `shoes` ");
            $temp = mysqli_fetch_array($count);
            $post = $temp[0];

            //Находим общее число страниц
            $total = (($post - 1) / $num) + 1;
            $total = intval($total);

            //Определим начало сообщений для текущей страницы
            $page = intval($page);
            //Если значение $page меньше единицы или отрицательно
            //переходим на первую страницы
            //А если слишком большое, то переходим на последнюю
            if (empty($page) or $page < 0) {
                $page = 1;
            }
            if ($page > $total) {
                $page = $total;
            }
            //Вычисляем начиная с какого номера
            //следует выводить сообщения
            $start = $page * $num - $num;
            $qury_start = "LIMIT $start, $num";


            if ($temp[0] > 0) {
                $result = mysqli_query($connection, "SELECT * FROM `shoes` WHERE 1 ORDER BY $sort $qury_start");
                if (mysqli_num_rows($result) > 0) {
                    $rows = mysqli_fetch_array($result);
                    do {
                        echo '
                          <div id="label">
                            <div id="picture">
                              <img id="img" src="/upload_images/'.$rows["IMAGE"].'"/>
                            </div>
                              <div id="title">
                                <p id="size"></p>
                              </div>

                                <div>
              						 <div id="product-price"><strong>'.$rows["NAME"].':'.$rows["PRICE"].' ₽</strong></div>
              					</div>
                                <div id="delivary">Кол-во: <input class="input-count" type="text" name="count_tovar" value="1"><span> шт.</span></div>
                                <a class="add-cart-style-list" data-id-wheel="'. $rows["ID"] .'" ></a>
                          </div>

                      ';
                    } while ($rows = mysqli_fetch_array($result));
                }
            }
            ?>
            <div id="footerfix">
            </div>
        </div>
    </div>
    <div id="input__tovar">
        <div class="product_section" id="section3">
            <div id="name_product"><p>Аксессуары</p></div>
            <a href="/product_accecssoreis.php"><div id="href_product">Ещё...</div></a>
            <?php
            $num = 3;
            $page = (int)$_GET['page'];

            $count = mysqli_query($connection, "SELECT COUNT(*) FROM `accessories` ");
            $temp = mysqli_fetch_array($count);
            $post = $temp[0];

            //Находим общее число страниц
            $total = (($post - 1) / $num) + 1;
            $total = intval($total);

            //Определим начало сообщений для текущей страницы
            $page = intval($page);
            //Если значение $page меньше единицы или отрицательно
            //переходим на первую страницы
            //А если слишком большое, то переходим на последнюю
            if (empty($page) or $page < 0) {
                $page = 1;
            }
            if ($page > $total) {
                $page = $total;
            }
            //Вычисляем начиная с какого номера
            //следует выводить сообщения
            $start = $page * $num - $num;
            $qury_start = "LIMIT $start, $num";


            if ($temp[0] > 0) {
                $result = mysqli_query($connection, "SELECT * FROM `accessories` WHERE 1 ORDER BY $sort $qury_start");
                if (mysqli_num_rows($result) > 0) {
                    $rows = mysqli_fetch_array($result);
                    do {
                        echo '
                          <div id="label">
                            <div id="picture">
                              <img id="img" src="/upload_images/'.$rows["IMAGE"].'"/>
                            </div>
                              <div id="title">
                                <p id="size"></p>
                              </div>

                                <div>
              						 <div id="product-price"><strong>'.$rows["NAME"].':'.$rows["PRICE"].' ₽</strong></div>
              					</div>
                                <div id="delivary">Кол-во: <input class="input-count" type="text" name="count_tovar" value="1"><span> шт.</span></div>
                                <a class="add-cart-style-list" data-id-wheel="'. $rows["ID"] .'" ></a>
                          </div>

                      ';
                    } while ($rows = mysqli_fetch_array($result));
                }
            }
            ?>
            <div id="footerfix">
            </div>
        </div>
    </div>



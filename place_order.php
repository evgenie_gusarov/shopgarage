<?php
require "/bd.php";
include("include/db_connect.php");
session_start();
?>
<!doctype html>
<html>
<head>
    <title>Интернет-магазин "Гараж.ру"</title>
    <meta charset = "utf-8">
    <link rel="stylesheet" type="text/css" href="css/store_location.css">
    <link rel="stylesheet" type="text/css" href="css/header.css">
    <link rel="stylesheet" type="text/css" href="/fonts/fonts.css">
    <script type="text/javascript" src="trackbar/jQuery/jquery-1.2.3.min.js"></script>
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="trackbar/jQuery/jquery.trackbar.js"></script>
</head>
<body>

<header>
    <?php
    include("/header.php")
    ?>
</header>

<div id="content_header_center">
    <h2 id="title_oplata">Оформление заказа: пошаговая инструкция</h2>
    <div>
        <h1 id="oplata">ШАГ 1: ВЫБОР ПРОДУКЦИИ ИЗ КАТАЛОГА И НАПОЛНЕНИЕ КОРЗИНЫ ЗАКАЗА</h1>

        <img src="https://avatars.mds.yandex.net/get-pdb/1088712/8b19d278-0b9b-46f8-89e4-66f5541efc55/s1200?webp=false">
        <p id="text_image"></p>
        </br>
        <p id="text_page"><b>Чтобы просмотреть категории каталога, наведите курсором мыши на раздел "Каталог" в верхнем горизонтальном меню.(рис. 1)</b></p>
        <p id="text_page"><b>Выберите нужную категорию и подраздел каталога.</b></p>
        <p id="text_page"><b>На открывшейся странице будут представлены карточки товаров, которые доступны к продаже. (рис. 2)</b></p>

        <img src="https://avatars.mds.yandex.net/get-pdb/1088712/8b19d278-0b9b-46f8-89e4-66f5541efc55/s1200?webp=false">
        <p id="text_image"></p>
        </br>
        <p id="text_page"><b>Чтобы просмотреть подробную информацию об интересующем Вас продукте, кликните на его изображение или название.</b></p>

        <img src="https://avatars.mds.yandex.net/get-pdb/1088712/8b19d278-0b9b-46f8-89e4-66f5541efc55/s1200?webp=false">
        <p id="text_image"></p>
        </br>
        <p id="text_page"><b>На странице с подробным описанием товара (рис. 3), перед тем как положить товар в корзину Вам необходимо, выбрать нужный размер1, укажите его количество2, цвет3, нажав на плюс для увеличения или на минус для уменьшения единиц товара.</b></p>
        <p id="text_page"><b>После того, как вы выбрали нужный размер, цвет, и ввели количество нажимайте кнопку «В корзину»4.</b></p>
    </div>
    <div>
        <h1 id="oplata">ШАГ 2: ПРОВЕРКА СОСТАВА ЗАКАЗА В КОРЗИНЕ</h1>

        <img src="https://avatars.mds.yandex.net/get-pdb/1088712/8b19d278-0b9b-46f8-89e4-66f5541efc55/s1200?webp=false">
        <p id="text_image"></p>
        </br>        <p id="text_page"><b>Чтобы просмотреть состав корзины, кликните на значок с изображением корзины или на сумму заказа в верхнем правом углу сайта (рис. 4). Цифра над значком обозначает количество добавленных в корзину товаров.</b></p>

        <p id="text_page"><b>Чтобы просмотреть подробную информацию об интересующем Вас продукте, кликните на его изображение или название.</b></p>

        <img src="https://avatars.mds.yandex.net/get-pdb/1088712/8b19d278-0b9b-46f8-89e4-66f5541efc55/s1200?webp=false">
        <p id="text_image"></p>
        </br>
        <p id="text_page"><b>В корзине можно изменить количество1 продуктов одного наименования, удалить2 ненужные или добавленные по ошибке товары. При внесении изменений данные автоматически обновляются.</b></p>
        <p id="text_page"><b>Обратите внимание! Минимальная сумма заказа: 5 000 рублей. Перед тем как переходить к оформлению заказа проверьте этот параметр. В частности, сумма заказа будет отображаться в конце таблицы корзины заказа.</b></p>
    </div>
    <div>
        <h1 id="oplata">ШАГ 3: ОФОРМЛЕНИЕ ЗАКАЗА</h1>

        <img src="https://avatars.mds.yandex.net/get-pdb/1088712/8b19d278-0b9b-46f8-89e4-66f5541efc55/s1200?webp=false">
        <p id="text_image"></p>
        </br>

        <p id="text_page"><b>Далее заполните профиль доставки, указав следующие данные:</b></p>
        <ul id="oplata_dostavka">
            <li>Ф.И.О.</li>
            <li>Адрес электронной почты</li>
            <li>Номер телефона</li>
            <li>Паспортные данные ( серию / номер )</li>
            <li>Адрес доставки ( индекс, город, адрес )</li>
        </ul>
        <p id="text_page"><b>Выбрать способ оплаты:</b></p>
        <ul id="oplata_dostavka">
            <li>По квитанции сбербанка</li>
            <li>Аанковской картой ( доступен только для физ. лица )</li>
        </ul>
        <p id="text_page"><b>Выбрать способ доставки:</b></p>
        <ul id="oplata_dostavka">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
    </div>
</div>
</body>
</html>

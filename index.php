<?php
require "/bd.php";
include("include/db_connect.php");
session_start();

$sorting = $_GET ["sort"];
switch ($sorting) {
    case 'price-asc':
        $sort = 'PRICE ASC';
        $sorting = 'price-asc';
        $sort_name = 'От дешевых к дорогим';
        break;

    case 'price-desc':
        $sort = 'PRICE DESC';
        $sorting = 'price-desc';
        $sort_name = 'От дорогих к дешевым';
        break;

    case 'no-sort':
        $sort = 'ID ASC';
        $sorting = 'price-desc';
        $sort_name = 'Нет сортировки';
        break;

    default:
        $sort = 'ID ASC';
        $sorting = '';
        $sort_name = 'Нет сортировки';
        break;
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>garage</title>
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/main_page.css">
    <link rel="stylesheet" href="/fonts/fonts.css">
    <link rel="icon" href="/images/logo.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/images/logo.ico" type="image/x-icon">
    <link href="trackbar/jQuery/trackbar.css" rel="stylesheet" type="text/css" />
    <link href="trackbar/trackbar.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="trackbar/jQuery/jquery-1.2.3.min.js"></script>
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="trackbar/jQuery/jquery.trackbar.js"></script>

</head>

<body>
<!-- Шапка сайта -->
<header>
    <?php
    include("/header.php")
    ?>
</header>

<!-- Тело главной страницы -->
<?php
include("/product_section.php");
?>

<script src="/js/index.js"></script>
<script src="/js/bucket.js"></script>
<script src="/js/shop-script.js"></script>
</body>
</html>

